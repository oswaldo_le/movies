const buscar = (schema) => {
    return async(ctx, next) => {
        try {
            await next()
            schema.validateAsync(ctx.request.header.y)
        } catch (err) {
            console.log((err) => console.log(err))
        }
    }
}
const lista = (schema) => {
    return async(ctx, next) => {
        try {
            await next()
            schema.validateAsync(ctx.request.header.p)
        } catch (err) {
            console.log((err) => console.log(err))
        }
    }
}
const body = (schema) => {
    return async(ctx, next) => {
        try {
            await next()
            schema.validateAsync(ctx.request.body)
        } catch (err) {
            console.log((err) => console.log(err))
        }
    }
}

module.exports = { lista, buscar, body }
const Router = require('koa-router')
const router = new Router()
const val = require('../middleware/validaciones')
const schBuscar = require('../schemas/buscar')
const schLista = require('../schemas/lista')
const schRemplazar = require('../schemas/remplazar')

const { buscar, lista, remplazar } = require('../controllers/controlador')

router.get('/', lista).get('/:t', buscar)

router.post('/remplazar', val.body(schRemplazar), remplazar)

module.exports = router
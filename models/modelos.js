const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate-v2')

const esquema = new Schema({
    imdbId: String,
    title: String,
    year: String,
    released: String,
    genre: [],
    director: [],
    actors: [],
    plot: String,
    ratings: [],
})
esquema.plugin(mongoosePaginate)

module.exports = mongoose.model('Modelo', esquema)
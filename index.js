const app = require('./app')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/peliculaDB', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

app.listen(3000)
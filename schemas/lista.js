const Joi = require('joi')

const schema = Joi.object({
    p: Joi.number().integer(),
})

module.exports = schema
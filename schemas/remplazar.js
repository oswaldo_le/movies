const Joi = require('joi')

const schema = Joi.object({
    title: Joi.string().required(),

    find: Joi.string().required(),

    replace: Joi.string().required(),
})

module.exports = schema
const Joi = require('joi')
const currentYear = new Date()

const schema = Joi.object({
    y: Joi.number().integer().max(currentYear.getFullYear()),
})

module.exports = schema
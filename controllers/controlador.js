const response = require('koa/lib/response')
const Movie = require('../models/modelos')
const omdb = new(require('omdbapi'))('47ec0ca4')

const buscar = async(ctx) => {
    ctx.body = 'buscar'
    const titulo = ctx.request.params.t
    const lanzamiento = ctx.request.header.y

    const busqueda = await omdb
        .get({
            title: titulo, // optionnal (requires  title)
            year: lanzamiento, // optionnal
        })
        .then((res) => {
            const busqueda = new Movie()
            busqueda.title = res.title
            busqueda.year = res.year
            busqueda.released = res.released
            busqueda.genre = res.genre
            busqueda.director = res.director
            busqueda.actors = res.actors
            busqueda.plot = res.plot
            busqueda.ratings = res.ratings

            let movieToUpdate = {}
            movieToUpdate = Object.assign(movieToUpdate, busqueda._doc)
            delete movieToUpdate._id

            const movie = Movie.findOneAndUpdate({ title: busqueda.title },
                movieToUpdate, { upsert: true, new: true }
            ).catch((err) => console.log(err))
            return (ctx.body = movieToUpdate)
        })
        .catch(console.error)
}

const lista = async(ctx) => {
    ctx.body = 'Lista'
    if (!ctx.request.header.p ||
        ctx.request.header.p == 0 ||
        ctx.request.header.p == '0' ||
        ctx.request.header.p == null ||
        ctx.request.header.p == undefined
    ) {
        var p = 1
    } else {
        var p = parseInt(ctx.request.header.p)
    }

    // opciones de paginacion
    var options = {
            limit: 5,
            page: p,
        }
        // paginado
    return await Movie.paginate({}, options).then((result) => {
        ctx.body = result
    })
}

const remplazar = async(ctx) => {
    const title = ctx.request.body.title
    const find = ctx.request.body.find
    const replace = ctx.request.body.replace

    if (title === '' || find === '' || replace === '') {
        throw console.log('Todos los campos deben estar llenos')
    }

    const remplazo = await Movie.findOne({ title: title })
        .lean()
        .then((response) => {
            const p = response.plot
            response.plot = p.replace(find, replace)

            const query = Movie.findOneAndUpdate({ title: title }, response, {
                new: true,
                upsert: false,
            }).then()
            ctx.body = response.plot
        })
        .catch((err) => console.log(err))
}

module.exports = {
    buscar,
    lista,
    remplazar,
}
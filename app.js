const Koa = require('koa')
const app = new Koa()
const router = require('./routes/rutas')
var bodyParser = require('koa-bodyparser')

app.use(bodyParser())
app.use(router.routes()).use(router.allowedMethods())

module.exports = app